

const addFile =require('./actions/add');
const listFile=require('./actions/list');
const getFile= require('./actions/get');
const removeFile= require('./actions/remove');
const clearFile= require('./actions/clear')

module.exports = {
    add: function(key,value){
        return addFile.add(key, value);
    }, list: function(){
        return listFile.list();
    }, get:function(key){
        return getFile.get(key);
    }, remove:function(key){
        return removeFile.remove(key);
    }, clear:function(){
        return clearFile.clear();
    }
};
require('make-runnable');