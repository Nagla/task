## Simple JS/Node Developer Challenge

### Description
This is a simple dictionary key/value store script with only core NodeAPI and ECMAScript.  
Node filesystem is used to Store the key/value dictionary.
The client should be a standalone terminal tool.

### Store Commands

`$ node store.js add mykey myvalue`

`$ node store.js list`

`$ node store.js get mykey`

`$ node store.js remove mykey`

`$ node store.js clear`

### Setup
1- Clone or download the repo,

2- Run 'npm install' to install 'make-runnable' package,

3- Write the commands above in the terminal to test te app,

4- you can check 'dict,json' to make sure that the commnd run correctly.
