const fs = require('fs');

module.exports.remove=function(key){
    fs.readFile('dict.json', function (err, data) {
        if (err) throw err;
        let json = JSON.parse(data);
        let index = json.findIndex(x => x[key]);
        if (index>-1){
            json.splice(index, 1);
            fs.writeFile("dict.json", JSON.stringify(json), function(err){
                if (err) throw err;
            })
            console.log("The key is removed successfully")
        }
        else{
            console.log("Can not remove element that does not exist")
        }
    })
}