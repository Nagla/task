const fs = require('fs');

module.exports.clear=function(){
    fs.readFile('dict.json', function (err, data) {
        if (err) throw err;
        let json = JSON.parse(data);
        if(json.length==0)
            console.log('The dictionary is already empty')
        else{
            json.length=0;
            fs.writeFile("dict.json", JSON.stringify(json), function(err){
                if (err) throw err;
            })
            console.log('The dictionary is cleared successfully')
        }
    })
}