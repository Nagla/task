const fs = require('fs');

module.exports.get=function(key){
    fs.readFile('dict.json', function (err, data) {
        if (err) throw err;

        let json = JSON.parse(data);
        let result = json.filter(x => x[key]);
        if(result.length>0)
            console.log(result);
        else
            console.log('key not found')
    })
}