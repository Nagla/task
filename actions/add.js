const fs = require('fs');

module.exports.add=function(key , value){
    let newObj={};
    newObj[key]= value;

    fs.readFile('dict.json', function (err, data) {
        if (err) throw err;
        let json = JSON.parse(data);
        json.push(newObj);
        console.log(json);

        fs.writeFile("dict.json", JSON.stringify(json), function(err){
            if (err) throw err;
        })
    })
}